function listener(details) {
  let filter = browser.webRequest.filterResponseData(details.requestId);
  let decoder = new TextDecoder("utf-8");
  let encoder = new TextEncoder();

  filter.ondata = event => {
    let str = decoder.decode(event.data, {stream: true});
    // Just remove the <meta http-equiv="refresh"> tag and all <script> elements from the HTTP response
    const metaRefreshRegex = /<meta http-equiv="refresh".*?>/g;
    const scriptRegex = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    str = str.replace(metaRefreshRegex, '').replace(scriptRegex, '');
    filter.write(encoder.encode(str));
    filter.disconnect();
  }

  return {};
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  {urls: ["*://www.drudgereport.com/*"], types: ["main_frame"]},
  ["blocking"]
);
